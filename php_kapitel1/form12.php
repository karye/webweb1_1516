<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8"/>
    <title>Formulär</title>
    <link rel="stylesheet" href="">
</head>
<body>
    <form action="./ovning22.php" method="post">
        <fieldset>
            <legend>Kontaktformulär</legend>

            Namn <input name="namn" type="text"><br>
            Epost <input name="epost" type="email"><br>
            Telefon <input name="telefon" type="tel"><br>

            Intresserad av nyhetsbrev <input name="nyhetsbrev" type="checkbox"><br>
            Vill bli kontaktad per telefon <input name="kontaktad" type="radio" value="telefon" checked><br>
            Vill bli kontaktad per epost <input name="kontaktad" type="radio" value="epost"><br>
            <input type="submit" value="Skicka">
        </fieldset>
    </form>
</body>
</html>
