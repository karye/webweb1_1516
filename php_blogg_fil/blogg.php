<!doctype html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Läsa en fil</title>
    <link rel="stylesheet" href="styles.css">
</head>

<body>
    <h1>Min blogg</h1>
    <?php
    $innehall = file("blogg.txt");

    foreach ($innehall as $rad) {
         echo "$rad";
    }
    ?>
</body>
</html>
