--
-- Tabellstruktur `bloggen2`
--

CREATE TABLE IF NOT EXISTS `bloggen2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tidstampel` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `rubrik` varchar(100) NOT NULL,
  `inlagg` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;
