-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Värd: localhost
-- Skapad: 08 apr 2016 kl 09:53
-- Serverversion: 5.5.47-MariaDB-1ubuntu0.14.04.1
-- PHP-version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Databas: `ryde_db`
--

-- --------------------------------------------------------

--
-- Tabellstruktur `bilar`
--

CREATE TABLE IF NOT EXISTS `bilar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reg` varchar(10) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `marke` varchar(50) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `modell` varchar(50) CHARACTER SET utf8 COLLATE utf8_swedish_ci DEFAULT NULL,
  `arsmodell` int(11) DEFAULT NULL,
  `pris` int(11) DEFAULT NULL,
  `agare` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agare` (`agare`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumpning av Data i tabell `bilar`
--

INSERT INTO `bilar` (`id`, `reg`, `marke`, `modell`, `arsmodell`, `pris`, `agare`) VALUES
(1, 'ABC123', 'Saab', '9-5', 2003, 130000, 1),
(3, 'DEF123', 'Volvo', 'S80', 2002, 140000, 2),
(4, 'DEF456', 'Toyota', 'Carina II', 1998, 30000, 2),
(5, 'GHI123', 'Mazda', '626', 2001, 80000, 3),
(6, 'JKL123', 'Audi', 'A8', 2001, 150000, 5),
(7, 'MNO123', 'BMW', '323', 1998, 60000, 5),
(8, 'PQR123', 'Ford', 'Mondeo', 2001, 90000, 4),
(9, 'STU123', 'Volvo', '740', 1987, 35000, 5),
(10, 'VYX123', 'Volkswagen', 'Golf', 1988, 40000, 5),
(11, 'XXX333', 'Nissan', 'Primera', 2003, 100000, NULL);

--
-- Restriktioner för dumpade tabeller
--

--
-- Restriktioner för tabell `bilar`
--
ALTER TABLE `bilar`
  ADD CONSTRAINT `bilar_ibfk_1` FOREIGN KEY (`agare`) REFERENCES `personer` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
