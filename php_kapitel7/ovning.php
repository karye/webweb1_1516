<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title>Regexp</title>
        <link rel="stylesheet" href="">
    </head>
    <body>
    <?php

        // Kollar om match: finns ordet 'jul'?
        $text = "Idag är det julavslutningen och vi börjar dej 7:e januari igen efter nyår";
        if (preg_match("/jul/", $text)) {
            echo "<p>Skönt att det är jul snart!</p>";
        } else {
            echo "<p>Du glömde julen!</p>";
        }

        // Finns någon av sifforna '0-9'?
        if (preg_match("/[0-9]/", $text)) {
            echo "<p>Bra att du kommer ihåg datumet vi börjar i januari.</p>";
        } else {
            echo "<p>Du glömde 7/1!</p>";
        }

        // Finns någon av bokstäverna gemena 'a-z'?
        $text1 = "SNART ÄR DET JUL IGEN!";
        if (preg_match("/[a-z]/", $text1)) {
            echo "<p>Skriv inte med stora bokstäver!</p>";
        } else {
            echo "<p>Najs! Din svensklärare är stolt över dig.</p>";
        }

        // Kollar att inga tal finns med i strängen (inverterat)
        if (preg_match("/[^0-9]/", $text)) {
            echo "<p>true</p>";
        } else {
            echo "<p>false</p>";
        }

        // Finns 'u' eller 'uu' eller 'uuu' ... ?
        $text2 = "Idag är det juulavslutningen och vi börjar dej 7:e januari igen efter nyår";
        if (preg_match("/u+/", $text2)) {
            echo "<p>true</p>";
        } else {
            echo "<p>false</p>";
        }

        // Matcha en eller flera förekomster av tecken med '+'
        if (preg_match("/u+/", $text2)) {
            echo "<p>true</p>";
        } else {
            echo "<p>false</p>";
        }

        // Matcha ett antal av tecken med '{}'
        $ip = "192.168.100.4";
        if (preg_match("/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/", $ip)) {
            echo "<p>Bra! Du har angett en giltig ip-adress.</p>";
        } else {
            echo "<p>Fel! Gå hem o plugga.</p>";
        }

        // Matcha på url-syntax
        $url = "http://twiggy/~ryde/kapitel7/ovning.php";
        if (preg_match("/^http:\/\/([^\/]+\/)+([a-z]+.php)$/", $url, $matcher)) {
            echo "<p>Detta är en korrekt url</p>";
            print_r($matcher);
        } else {
            echo "<p>Fel syntax på din url</p>";
        }

        // Byta ut text i en sträng
        $url2 = "https://www.lysator.liu.se/history/garb/txt/88-2-tomten.txt";

        // Hämta text från en url
        $text3 = file_get_contents($url2);

        // Omkodar till utf-8 (från Windows-teckenkod)
        $text3 = utf8_encode($text3);

        // Byter ut texten 'nissar' med texten 'cyberbarn'
        $text3 = preg_replace("/nissar/", "cyberbarn", $text3);

        // Byter ut dubber radslut '\n\n' med till paragrafer
        $text3 = preg_replace("/\n\n/", "</p><p>", $text3);

        // Och byter enkel radslut med '<br>'
        $text3 = preg_replace("/\n/", "<br>\n", $text3);
        echo "<p>$text3</p>";
    ?>
    </body>
</html>
