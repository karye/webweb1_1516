<?php
/**
    * Enkel skatteberäkning
    * PHP version 5
    * @category
    * @package
    * @author     Karim Ryde <karye.webb@gmail.com>
    * @license    PHP CC
    * @link       http://twiggy/~ryde/..
    */

/* Stäng av felmeddelanden */
ini_set("display_errors", 1);

phpversion();
phpinfo();
