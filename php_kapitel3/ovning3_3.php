<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="">
    </head>
    <body>
<?php
    if (!empty($_REQUEST['tal1']) && !empty($_REQUEST['tal2'])) {
        $tal1 = $_REQUEST['tal1'];
        $tal2 = $_REQUEST['tal2'];

        // Sorterar talen - låg och hög
        if ($tal1 > $tal2) {
            $low = $tal2;
            $high = $tal1;
        } else {
            $low = $tal1;
            $high = $tal2;
        }

        // Skriv ut alla heltal mellan tal1 och tal2
        for ($i=$low+1; $i<$high; $i++) {
            echo "<p>$i</p>";
        }
    } else {
        echo "<h2>Ingen data mattades in!</h2>";
?>
       <form action="ovning3_3.php" method="post">
           <label>Tal 1</label><input type="number" name="tal1"><br>
           <label>Tal 2</label><input type="number" name="tal2"><br>
           <input type="submit" value="Skicka">
       </form>
<?php
    }
?>
    </body>
</html>
