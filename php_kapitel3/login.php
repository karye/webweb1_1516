<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title>Ett tjusigt formulär</title>
    <link rel="stylesheet" href="login.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>

<body>
    <div id="wrapper">
    <?php
    // Kontrollera om vi får data eller inte
    if (!empty($_REQUEST['username']) && !empty($_REQUEST['password'])) {

        // Plocka ut datat
        $username = $_REQUEST['username'];
        $password = $_REQUEST['password'];

        if ($username == "bjorn" && $password == "webb") {
            echo "<h1>Välkommen tillbaka $username</h1>";

        }
    } else {
?>
            <form name="login-form" class="login-form" action="login.php" method="post">
                <header>
                    <h1>Inloggninsformulär</h1>
                    <p>Mata in dina inloggningsuppgifter i detta koola formulär</p>
                </header>
                <main>
                    <div class="row">
                        <input name="username" type="text" class="input username" placeholder="Användarnamn" />
                        <div class="user-icon"></div>
                    </div>
                    <div class="row">
                        <input name="password" type="password" class="input password" placeholder="Lösenord" />
                        <div class="pass-icon"></div>
                    </div>
                </main>
                <footer>
                    <input type="submit" name="submit" value="Logga in" class="button" />
                    <input type="submit" name="submit" value="Registrera" class="register" />
                </footer>
            </form>

            <?php
        }
        ?>
    </div>
</body>

</html>
