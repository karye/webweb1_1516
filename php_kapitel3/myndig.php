<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>

<body>

    <form action="myndig.php" method="post">
        <fieldset>
            <legend>Är du myndig?</legend>
            <label>Födelsedatum</label>
            <input type="date" name="alder">
            <br>
            <input type="submit" value="Kolla">
        </fieldset>
    </form>
    <?php
    if (!empty($_REQUEST['alder'])) {
        $alderObjekt = new datetime($_REQUEST['alder']);
        $idagObjekt = new datetime();
        $alder = $idagObjekt->diff($alderObjekt)->y;

        if ( $alder >= 18) {
            echo "<p>Du är $alder år och myndig!</p>";
        } else {
            echo "<p>Du är $alder år och omyndig!</p>";
        }
    }
    ?>
</body>

</html>
