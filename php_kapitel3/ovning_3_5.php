<?php
/**
    * Lånekalkylator
    * PHP version 5
    * @category   Enkel skriptsida
    * @package    lånekalkylator
    * @author     Karim Ryde <karye.webb@gmail.com>
    * @license    PHP CC
    * @link       http://twiggy/~ryde/..
    */

/* Stäng av felmeddelanden */
ini_set('display_errors', 'Off');
?>
<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title>Lånekalkylator</title>
        <link rel="stylesheet" href="">
    </head>
    <body>
<?php
    /* Tar emot data och kontrollerar att det är inte tomt */
    if (!empty($_REQUEST['tid']) && !empty($_REQUEST['belopp']) && !empty($_REQUEST['ranta'])) {

        /* Hämta ut data */
        $tid = $_REQUEST['tid'];
        $belopp = $_REQUEST['belopp'];
        $ranta = $_REQUEST['ranta'];
        $startbelopp = $belopp;

        /* Våra beräkningar */
        for ($i = 1; $i <= $tid ; $i++) {
            $belopp = $belopp * (1 + $ranta/100);
        }
        $lanekostnad = $belopp - $startbelopp;

        echo "<p>Totala lånekostnaden är $lanekostnad</p>";

    } else {
        echo "<h2>Ingen data mattades in!</h2>";
?>
        <!-- Formulär för att mata inte värden -->
        <form action="ovning_3_5.php" method="post">
            <label>Lånetid</label>
            <input type="radio" name="tid" value="1">1 år
            <input type="radio" name="tid" value="3">3 år
            <input type="radio" name="tid" value="5">5 år<br>
            <label>Belopp</label><input type="number" name="belopp"><br>
            <label>Ränta</label><input type="number" name="ranta"><br>
            <input type="submit" value="Beräkna">
        </form>
<?php
    }
?>
    </body>
</html>
