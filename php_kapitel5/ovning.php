<?php
/* Första sättet att skapa en arrary */
$kokskap[0] = "bestick";
$kokskap[1] = "servetter";
$kokskap[2] = "påsar";

/* Andra sättet att skapa en array */
$kokskap1 = ["bestick", "servetter", "påsar"];

/* Tredje sättet att skapa en array */
$kokskap2 = array("bestick", "servetter", "påsar");

/* Fjärde sättet att skapa en array */
$elever[] = "Jakob";
$elever[] = "Jesper";
$elever[] = "Badr";
$elever[] = "Patrik";
$elever[] = "Lucas";
$elever[] = "Felix";
$elever[] = "Alexander";
$elever[] = "Simon";
$elever[] = "Jessica";
$elever[] = "Mattias";

/* Femte sättet att skapa en array - associativ */
$personNr["Jesper"] = "970625";
$personNr["Jakob"] = "971005";
$personNr["Afshin"] = "970213";
$personNr["Simon"] = "970620";
$personNr["Jessica"] = "970528";
?>
<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="">
    </head>
    <body>
<?php
        print_r($kokskap);
        echo "<br>";
        print_r($kokskap1);

        echo "<p>Element på position 1 = $kokskap[1]</p>";
        echo "<p>Element på position 2 = $kokskap[2]</p>";

        /* Iterera över arrayen */
        foreach ($kokskap as $item) {
            echo "<p>$item</p>";
        }

        foreach ($elever as $elev) {
            echo "<p>$elev</p>";
        }

        foreach ($personNr as $index => $nr) {
            echo "<p>$index har personnummer $nr</p>";
        }
        /* Sortera en array */
        sort($personNr);
        foreach ($personNr as $index => $nr) {
            echo "<p>$index har personnummer $nr</p>";
        }

        $allaElever = implode(" ", $elever);
        echo "<p>$allaElever</p>";

        $text = "Idag är det luciatåg på skolan";
        $ord = explode(" ", $text);
        print_r($ord);

        $url = "http://php.net/manual/en/function.explode.php";
        $urlDelar = explode("/", $url);
        print_r($urlDelar);


?>
    </body>
</html>
