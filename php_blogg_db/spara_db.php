<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Spara i tabellen</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
    // Databasuppgifter
    $hostname = 'localhost';
    $user = 'ryde_user';
    $password = 'ryde_pass';
    $database = 'ryde_db';

    // Anslut till databasen
    $conn = new mysqli($hostname, $user, $password, $database);

    // Om någonting går fel. Avsluta och skriv ett felmeddelandet
    if ($conn->connect_error)
        die("Kunde inte ansluta till databasen: " . $conn->connect_error);

    // Gör om radslut till taggen <br>
    $inlagg = nl2br($_POST['inlagg'], false);

    // Vårt sql-kommando
    $query = "INSERT INTO bloggen2 (inlagg) VALUES ('$inlagg');";

    // Kör sql
    $result = $conn->query($query);

    // Gick det bra? Om inte skriv felmeddelande
    if (!$result)
        die("Kunde inte spara inlägg: " . $conn->error);
    else
        echo "<h3>Inlägget registrerat!</h3>";

    // Stäng ned databasanslutningen
    $conn->close();
    ?>
</body>
</html>
