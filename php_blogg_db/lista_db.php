<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <title>Min blogg!</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <?php
    // Databasuppgifter
    $hostname = 'localhost';
    $user = 'ryde_user';
    $password = 'ryde_pass';
    $database = 'ryde_db';

    // Anslut till databasen
    $conn = new mysqli($hostname, $user, $password, $database);

    // Om någonting går fel. Avsluta och skriv ett felmeddelandet
    if ($conn->connect_error)
        die("Kunde inte ansluta till databasen: " . $conn->connect_error);

    // Vårt sql-kommando
    $query = "SELECT * FROM bloggen2;";

    // Kör sql
    $result = $conn->query($query);

    // Gick det bra? Om inte skriv ut felmeddelande
    if (!$result)
        die("Kunde inte hämta inlägg: " . $conn->error);

    echo "<h1>Lista på alla inlägg</h1>";

    // Berätta hur många poster vi hittat som motsvara sökvillkoret
    echo "<p>Hittat " . $result->num_rows . " poster i tabellen</p>";

    echo "<table>";

    // Skriv ut alla inlägg en efter en
    while ($row = $result->fetch_assoc()) {
        echo "<tr>";
        echo "<td>" . $row['tidstampel'] . "</td>";
        echo "<td>" . substr($row['rubrik'], 0, 10) . "</td>";
        echo "<td>" . substr($row['inlagg'], 0, 30) . "</td>";
        echo "</tr>";
    }

    echo "</table>";

    // Stäng ned databasanslutningen
    $result->free();
    $conn->close();
    ?>
</body>
</html>
