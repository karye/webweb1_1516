<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" href="">
</head>

<body>
<?php
    /* Kontroll: kommer in något? */
    if (isset($_REQUEST['minText'])) {
        $minText = $_REQUEST['minText'];

        /* Med trim() tar vi bort mellanslag
           i början och slutet */
        $trimmed = trim($minText);

        echo "<p>.$trimmed.</p>";
    } else {
        echo "<p>Du har inte matat in något!</p>";
    }
?>
</body>

</html>
