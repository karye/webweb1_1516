<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8" />
    <title></title>
    <link rel="stylesheet" href="">
</head>

<body>
    <?php
    // Ta emot data
    $temp = $_REQUEST['temp'];
    $typ = $_REQUEST['typ'];
    //echo "<p>typ= $typ</p>";

    if ($typ == 'C') {
        // Omvandling från Celsius till Farenheit
        $fahrenheit = (9/5) * $temp + 32;
        echo "<p>$temp grader Celsius motsvarar $fahrenheit grader Fahrenheit</p>";
    } else {
        // Omvandling från Fahrenheit till Celsius
        $celsius = 5/9 * ($temp - 32);
        echo "<p>$temp grader Fahrenheit motsvarar $celsius grader Celsius</p>";
    }
?>
</body>

</html>
