<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8" />
    <title></title>
    <link rel="stylesheet" href="">
</head>

<body>
    <?php
    // Ta emot data
    $texten = $_REQUEST['texten'];
    $typ = $_REQUEST['typ'];

    switch ($typ) {
        case 'V':
            // Omvandla texten till versaler
            $texten = strtoupper($texten);
            echo "<p>Till versaler: $texten</p>";
            break;
        case 'G':
            // Omvandla texten till gemener
            $texten = strtolower($texten);
            echo "<p>Till gemener: $texten</p>";
            break;
        case 'L':
            // Räkna ut längden på strängen
            $lengd = strlen($texten);
            echo "<p>Texten är $lengd tecken lång.</p>";
            break;
    }
?>
</body>

</html>
