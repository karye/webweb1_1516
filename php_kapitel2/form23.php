<!DOCTYPE html>
<html lang="sv">

<head>
    <meta charset="utf-8" />
    <title>Formulär</title>
    <link rel="stylesheet" href="">
</head>

<?php
    // Kontrollera att $_REQUEST['farg'] finns
    // Första skriptet körs får vi ingen färg
    // Då sätter färgen till svart
    if (isset($_REQUEST['farg']))
        $farg = $_REQUEST['farg'];
    else
        $farg = "black";

    // Skriv body-elementet
    echo "<body style=\"background:$farg;\">";
?>
    <form action="./form23.php" method="post">
        <fieldset>
            <legend>Ange bakgrundsfärg</legend>
            Färg
            <select name="farg">
                <option value="red">Röd</option>
                <option value="blue">Blå</option>
                <option value="green">Grön</option>
                <option value="yellow">Gul</option>
            </select>
            <br>
            <input type="submit" value="Måla bakgrund">
        </fieldset>
    </form>
</body>

</html>
