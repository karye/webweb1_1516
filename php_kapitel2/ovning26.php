<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8"/>
    <title></title>
    <link rel="stylesheet" href="">
</head>
<body>
<?php
    // Ta emot data
    $celsius = $_REQUEST['temp'];

    // Omvandling från Celsius till Farenheit
    $kelvin = round($celsius - 273.15, 2);

    // Skriv ut resultat
    echo "<p>$celsius grader Celsius motsvarar $kelvin grader Kelvin</p>";
?>
</body>
</html>
