<?php
function print_table_start($farg) {
    echo "<table style=\"background-color:$farg\">";
}
function print_table_row($cell1, $cell2) {
    echo   "<tr>
            <td>$cell1</td><td>$cell2</td>
            </tr>";
}
function print_table_end() {
    echo "</table>";
}
?>
<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="">
    </head>
    <body>
<?php
    print_table_start("red");
    print_table_row("Fredag", "Lördag");
    print_table_end();
?>
    </body>
</html>
