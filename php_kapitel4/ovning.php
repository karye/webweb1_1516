<?php

/* Skapa funktionen */
function fetstil($text = "Fet stil", $farg = "red") {
    echo "<p style='font-weight: bold; color: $farg;'>$text</p>";
}

/* Funktion som räknar ut arean på en cirkel */
function areaCirkel($radie) {
    $area = pi() * $radie * $radie;
    return $area;
}

/* Funktion som räknar ut summan av två tal */
function summa($tal1, $tal2) {
    $summa = $tal1 + $tal2;
    return $summa;
}

/* Funktion som räknar summan av valfritt antal tal */
function summa2($talArray) {
    $summa = 0;
    foreach ($talArray as $tal) {
        $summa = $summa + $tal;
    }
    return $summa;
}

function dubbel($tal) {
    $tal = $tal * 2;
    return $tal;
}
function dubbel2() {
    global $tal;
    $tal = $tal * 2;
    return $tal;
}

?>
<!DOCTYPE html>
<html lang="sv">
    <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="">
    </head>
    <body>
<?php
    /* Använder funktionen */
    fetstil();
    fetstil("Karim Ryde", "blue");
    fetstil("Funktioner fungerar ungefär som ett snabbkommando för att göra en lång lista av kommandon i ett svep. Därmed slipper man upprepa kod i programmet, vilket gör det lättare att skriva och uppdatera. Förutom de många funktioner som php har inbyggt, kan man alltså skapa egna funktioner.", "pink");
    fetstil("Tomte", "mistyrose", 25);

    echo "<p>" . areaCirkel(5) . "</p>";
    echo "<p>" . summa(5, 7) . "</p>";

    echo "<p>" . summa2([2,3,6,8,9]) . "</p>";

    echo "<p>" . dubbel(6) . "</p>";
    $tal = 3;
    echo "<p>" . dubbel(6) . "</p>";

    $tal = 6;
    echo "<p>" . dubbel2() . "</p>";

?>
    </body>
</html>
